### Instructions:

1. Open terminal, give the executable perms
`chmod +x gcp_init.sh`
`chmod +x gcp_transfer.sh`

2. run `./gcp_init`: Upon prompt
    - choose yes [Y]
    - choose minecraft villy project [1]
    - choose server region [4]

### File Transfer to the GCP vm instance 

gcp_transfer usage: `./gcp_transfer.sh <FILENAME> <OPTIONAL DESTINATION>`
    - Ex: `./gcp_transfer.sh target/villy-0.jar`